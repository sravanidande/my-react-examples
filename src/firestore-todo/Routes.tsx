import React from 'react'
import {
  BrowserRouter,
  Redirect,
  Route,
  RouteComponentProps,
} from 'react-router-dom'
import { FSAddTodo, FSEditTodo, FSTodoList } from '../firestore-todo'

const EditTodo: React.FC<RouteComponentProps<{ readonly id: string }>> = ({
  match,
}) => <FSEditTodo id={match.params.id} />

export const FSTodoApp = () => (
  <BrowserRouter>
    <Route exact path="/todos/add" component={FSAddTodo} />
    <Route exact path="/todos/:id/edit" component={EditTodo} />
    <Route exact path="/todos" component={FSTodoList} />
    <Route exact path="/" render={() => <Redirect to="/todos" />} />
  </BrowserRouter>
)
