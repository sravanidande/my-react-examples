import React from 'react'
import { RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import {
  Async,
  Table,
  TableBody,
  TableHead,
  Th,
  Tr,
} from 'technoidentity-devfractal'
import { FSTodoItem } from './FSTodoItem'
import { all, FSTodo, remove } from './todoAPI'

export interface FSTodoListProps {
  readonly todoList: ReadonlyArray<FSTodo>
  onDeleteTodo(id: string): void
  onEditTodo(id: string): void
}

export const FSTodoListView: React.SFC<FSTodoListProps> = ({
  todoList,
  onDeleteTodo,
  onEditTodo,
}) => {
  return (
    <>
      <Link to="/todos/add">AddTodo</Link>
      <Table>
        <TableHead>
          <Tr>
            <Th>ID</Th>
            <Th>Title</Th>
            <Th>Done</Th>
          </Tr>
        </TableHead>
        <TableBody>
          {todoList.map(todo => (
            <FSTodoItem
              key={todo.id}
              todo={todo}
              onDeleteTodo={onDeleteTodo}
              onEditTodo={onEditTodo}
            />
          ))}
        </TableBody>
      </Table>
    </>
  )
}

const useUpdate = () => {
  const [toggle, set] = React.useState(false)
  const update = () => {
    set(!toggle)
  }
  return update
}

export const FSTodoList: React.FC<RouteComponentProps> = ({ history }) => {
  const update = useUpdate()
  const handleDelete = async (id: string) => {
    await remove(id)
    // console.log('handleDelete')
    update()
  }

  const handleEditTodo = (id: string) => {
    history.push(`/todos/${id}/edit`)
  }

  // console.log('render')
  return (
    // tslint:disable-next-line: no-unnecessary-callback-wrapper
    <Async asyncFn={async () => all()}>
      {({ isLoading, error, data }) => {
        if (isLoading) {
          return <h1>Loading...</h1>
        }
        if (data) {
          return (
            <FSTodoListView
              todoList={data}
              onDeleteTodo={handleDelete}
              onEditTodo={handleEditTodo}
            />
          )
        }
        return <h1>{`${error}`}</h1>
      }}
    </Async>
  )
}
