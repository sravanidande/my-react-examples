import React from 'react'
import {
  Inspector,
  useBooleanKnob,
  useColorKnob,
  useNumberKnob,
  useObjectKnob,
  useRangeKnob,
  useSelectKnob,
  useTextKnob,
  useTimeMachine,
} from 'retoggle'

export const TextKnob: React.FC = () => {
  const [text] = useTextKnob('Name', 'Hello world')
  return (
    <>
      <Inspector />
      <h1>{text}</h1>
    </>
  )
}

export const NumberKnob: React.FC = () => {
  const [numb] = useNumberKnob('Number', 10)
  return (
    <>
      <Inspector />
      {numb}
    </>
  )
}

export const BooleanKnob: React.FC = () => {
  const [value] = useBooleanKnob('Boolean', true)
  return (
    <>
      <Inspector />
      {value.toString()}
    </>
  )
}

export const SelectKnob: React.FC = () => {
  const [selectedOption] = useSelectKnob(
    'select',
    ['red', 'pink', 'green'],
    'pink',
  )
  return (
    <>
      <Inspector />
      {selectedOption}
    </>
  )
}

export const RangeKnob: React.FC = () => {
  const [range] = useRangeKnob('Range', {
    initialValue: 20,
    min: 40,
    max: 100,
  })
  return (
    <>
      <Inspector />
      {range}
    </>
  )
}

export const ObjectKnob: React.FC = () => {
  const [obj] = useObjectKnob('Fruits', {})
  return (
    <>
      <Inspector />
      {JSON.stringify(obj)}
    </>
  )
}

export const ColorKnob: React.FC = () => {
  const [color] = useColorKnob('Color', 'green')
  return (
    <>
      <Inspector />
      {color}
    </>
  )
}

export const TimeMachineKnob: React.FC = () => {
  const [state, setState] = React.useState('Hello')
  const timeTravelledState = useTimeMachine('State timemachine', state)
  return (
    <>
      <Inspector />
      <input
        type="text"
        value={timeTravelledState}
        onChange={e => setState(e.target.value)}
      />
      {timeTravelledState}
    </>
  )
}
