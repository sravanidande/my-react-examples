import axios from 'axios'
import React from 'react'
import { GithubUserInfo } from './types'

export interface CardViewProps {
  readonly userInfo: GithubUserInfo
}

export const CardView: React.SFC<CardViewProps> = ({ userInfo }) => (
  <div className="card">
    <div className="card-content">
      <div className="card-image">
        <img src={userInfo.avatarUrl} alt="avatar" />
      </div>
      <p className="title">{userInfo.name}</p>
      <p className="sub-title">{userInfo.blog}</p>
    </div>
  </div>
)

export interface CardProps {
  readonly name: string
}

export const Card: React.SFC<CardProps> = ({ name }) => {
  const [userInfo, setUserInfo] = React.useState<GithubUserInfo | undefined>(
    undefined,
  )

  React.useEffect(() => {
    // tslint:disable-next-line: no-floating-promises
    axios
      .get(`https://api.github.com/users/${name}`)
      // tslint:disable-next-line:no-console
      .then(resp => resp.data)
      .then(data =>
        setUserInfo({
          name: data.name,
          blog: data.blog,
          avatarUrl: data.avatar_url,
        }),
      )
  })

  if (userInfo === undefined) {
    return <h1>Get User....</h1>
  } else {
    return <CardView userInfo={userInfo} />
  }
}
