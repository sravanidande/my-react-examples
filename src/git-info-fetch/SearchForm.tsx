import React from 'react'
import { Simple } from 'technoidentity-devfractal'

export interface SearchFormProps {
  onSearch(name: string): void
}

export const SearchForm: React.SFC<SearchFormProps> = ({ onSearch }) => {
  return (
    <Simple.Form
      initialValues={{ name: '' }}
      onSubmit={(values, actions) => {
        onSearch(values.name)
        actions.setSubmitting(false)
      }}
    >
      <Simple.Text name="name" />
      <Simple.FormButtons />
    </Simple.Form>
  )
}
