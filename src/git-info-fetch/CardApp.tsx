import React from 'react'
import { Card } from './Card'
import { SearchForm } from './SearchForm'

export const CardApp: React.FC = () => {
  const [name, setName] = React.useState('')

  const handleSearch = (userName: string) => {
    setName(userName)
  }

  return (
    <>
      <SearchForm onSearch={handleSearch} />
      <Card name={name} />
    </>
  )
}
