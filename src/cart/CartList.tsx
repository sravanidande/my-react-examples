import React from 'react'
import {
  Table,
  TableBody,
  TableHead,
  Td,
  Text,
  Th,
  Title,
  Tr,
} from 'technoidentity-devfractal'
import { Product } from './Product'

interface CartItem {
  readonly product: Product
  readonly count: number
}
export interface CartItemProps {
  readonly cartItem: CartItem
}

export const CartItem: React.SFC<CartItemProps> = ({ cartItem }) => {
  return (
    <Tr>
      <Td>{cartItem.product.id}</Td>
      <Td>{cartItem.product.title}</Td>
      <Td>{cartItem.product.price}</Td>
      <Td>{cartItem.count}</Td>
      <Td>{cartItem.product.price * cartItem.count}</Td>
    </Tr>
  )
}

export const totalPrice = (cart: ReadonlyArray<CartItem>): number =>
  cart.reduce((acc, ci) => acc + ci.count * ci.product.price, 0)

export interface CartListProps {
  readonly cartList: ReadonlyArray<CartItem>
}

export const CartList: React.SFC<CartListProps> = ({ cartList }) => {
  return (
    <>
      <Title>Cart</Title>
      <Table>
        <TableHead>
          <Tr>
            <Th>Id</Th>
            <Th>Title</Th>
            <Th>Price</Th>
            <Th>Count</Th>
            <Th>Total Price</Th>
          </Tr>
        </TableHead>
        <TableBody>
          {cartList.map((item, index) => (
            <CartItem key={index} cartItem={item} />
          ))}
        </TableBody>
      </Table>
      <Text>Total Amount:{totalPrice(cartList)}</Text>
    </>
  )
}
