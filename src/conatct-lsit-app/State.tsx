import React from 'react'
import { ContactList } from './ContactList'

export const ContactState: React.FC = () => {
  const [contacts, setContacts] = React.useState<any[]>([])

  React.useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(setContacts)
      .catch(err => err.name)
  }, [])

  return <ContactList contactList={contacts} />
}
