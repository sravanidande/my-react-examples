import React from 'react'

export interface ContactProps {
  readonly name: string
  readonly email: string
  readonly text: string
}

export const Contact: React.SFC<ContactProps> = ({ name, email, text }) => (
  <div>
    <div className="card">
      <div className="card-content">
        <p className="title">{name}</p>
        <p className="sub-title is-4">{email}</p>
        <p className="sub-title is-6">{text}</p>
      </div>
    </div>
  </div>
)

export interface ContactListProps {
  readonly contactList: ReadonlyArray<ContactProps>
}

export const ContactList: React.SFC<ContactListProps> = ({ contactList }) => {
  return (
    <>
      {contactList.map(contact => (
        <Contact
          key={contact.name}
          name={contact.name}
          email={contact.email}
          text={contact.text}
        />
      ))}
    </>
  )
}
