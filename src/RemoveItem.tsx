import React from 'react'
import {
  Button,
  Table,
  TableBody,
  TableHead,
  Td,
  Th,
  Tr,
} from 'technoidentity-devfractal'
import { useImmer } from 'use-immer'

interface ListItem {
  readonly id: string
  readonly name: string
}

interface ListItems {
  readonly listItems: ReadonlyArray<ListItem>
  handleDelete(id: string): void
}
export const initialList: ReadonlyArray<ListItem> = [
  { id: 'a', name: 'Learn React' },
  { id: 'b', name: 'Learn Firebase' },
  { id: 'c', name: 'Learn GraphQL' },
]

export const ItemsList: React.FC<ListItems> = ({ listItems, handleDelete }) => {
  return (
    <Table>
      <TableHead>
        <Tr>
          <Th>Id</Th>
          <Th>Name</Th>
        </Tr>
      </TableHead>
      <TableBody>
        {listItems.map((item, index) => (
          <Tr key={index}>
            <Td>{item.id}</Td>
            <Td>{item.name}</Td>
            <Td>
              <Button variant="danger" onClick={() => handleDelete(item.id)}>
                Delete
              </Button>
            </Td>
          </Tr>
        ))}
      </TableBody>
    </Table>
  )
}

export const RemoveItem: React.FC = () => {
  const [list, setList] = useImmer(initialList)
  const handleDelete = (id: string) => {
    setList(draft => {
      const oneItem = draft.findIndex(item => id === item.id)
      draft.splice(oneItem, 1)
    })
  }
  return <ItemsList listItems={list} handleDelete={handleDelete} />
}
