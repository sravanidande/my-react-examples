1. ~~[Using Custom React Hooks to Simplify Forms - Upmostly](https://upmostly.com/tutorials/using-custom-react-hooks-simplify-forms/)~~

2. ~~[Combining Arrays Without Duplicates ― The Ultimate Guide to JavaScript Algorithms ― Scotch.io](https://scotch.io/courses/the-ultimate-guide-to-javascript-algorithms/combining-arrays-without-duplicates)~~
3. ~~[React 101 - A Practical Introduction](https://sebhastian.com/learning-the-basics-of-React-by-doing)~~
4. ~~[How to use useReducer in React Hooks for performance optimization](https://medium.com/crowdbotics/how-to-use-usereducer-in-react-hooks-for-performance-optimization-ecafca9e7bf5)~~
5. ~~[Write your first React Hook! - Hashnode](https://hashnode.com/post/write-your-first-react-hook-cjrt8lfci00aw18s1z8v9s06n)~~
6. [How To Write a Search Component with Suggestions in React - DEV Community ](https://dev.to/sage911/how-to-write-a-search-component-with-suggestions-in-react-d20)
7. ~~[f/react-wait: Complex Loader Management Hook for React Applications](https://github.com/f/react-wait)~~
8. [Build a live map application with React](https://pusher.com/tutorials/live-map-react)
9. ~~[ReactJS — A quick tutorial to build dynamic JSON based form.](https://codeburstreactjs-a-quick-tutorial-to-build-dynamic-json-based-form-a4768b3151c0)~~
10. [A React simple app example: fetch GitHub users information via API](https://flaviocopes.com/react-example-githubusers/)
11. [The Modern Javascript Tutorial](https://javascript.info/)
12. ~~[Animation in React](https://www.nearform.com/blog/animation-in-react/)~~
13. ~~[A guide to JavaScript Regular Expressions](https://flaviocopes.com/javascript-regular-expressions/)~~
14. ~~[React Hooks for firebase](https://github.com/csfrequency/react-firebase-hooks)~~
15. [Beautiful and Accessible Drag and Drop with react-beautiful-dnd from @alexandereardon on @eggheadio](https://egghead.io/courses/beautiful-and-accessible-drag-and-drop-with-react-beautiful-dnd)
16. ~~[React Simple Animate - UI Animation made simple](https://react-simple-animate.now.sh/)~~
17. ~~[How to use Firebase Realtime Database in React - RWieruch](https://www.robinwieruch.de/react-firebase-realtime-database/)~~
18. ~~[Hooks - useAnimate and useAnimateKeyframes with react simple animate](https://react-simple-animate.now.sh/hooks)~~
19. [react-spring](https://www.react-spring.io/docs/hooks/use-spring)
20. ~~[Git hacks you should know about - DEV Community](https://dev.to/teamxenox/git-hacks-you-should-know-about-16pk)~~
21. ~~[Getting started with GraphQL and TypeScript](https://pusher.com/tutorials/graphql-typescript)~~
22. ~~[Runtime type checking with io-ts in Typescript – Otto Kivikärki – Medium](https://medium.com/@ottoki/runtime-type-checking-with-io-ts-in-typescript-14465169fb02)~~
