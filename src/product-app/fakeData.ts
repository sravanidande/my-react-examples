import Chance from 'chance'

const chance: Chance.Chance = new Chance()

export const fakeID: () => number = () =>
  chance.integer({ min: 100, max: 1000 })

export const fakeTitle: () => string = () => chance.sentence({ words: 3 })

export const fakePrice: () => number = () =>
  chance.floating({ fixed: 3, min: 0, max: 1000 })

export interface FakeProductType {
  readonly id: number
  readonly title: string
  readonly price: number
}

export const fakeProducts: () => Promise<
  ReadonlyArray<FakeProductType>
> = async () =>
  Object.freeze([
    { id: fakeID(), title: fakeTitle(), price: fakePrice() },
    { id: fakeID(), title: fakeTitle(), price: fakePrice() },
    { id: fakeID(), title: fakeTitle(), price: fakePrice() },
  ])
