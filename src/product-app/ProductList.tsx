import React from 'react'
import { Table, TableBody, TableHead, Th, Tr } from 'technoidentity-devfractal'
import { FakeProductType } from './fakeData'
import { Product } from './Product'

export interface ProductListProps {
  readonly productList: ReadonlyArray<FakeProductType>
}

export const ProductList: React.SFC<ProductListProps> = ({ productList }) => {
  return (
    <Table>
      <TableHead>
        <Tr>
          <Th>ID</Th>
          <Th>Title</Th>
          <Th>Price</Th>
        </Tr>
      </TableHead>
      <TableBody>
        {productList.map(product => (
          <Product key={product.id} product={product} />
        ))}
      </TableBody>
    </Table>
  )
}
