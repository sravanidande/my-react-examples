import React from 'react'
import { Button, Td, Tr } from 'technoidentity-devfractal'
import { FakeProductType } from './fakeData'

export interface ProductProps {
  readonly product: FakeProductType
}

export const Product: React.SFC<ProductProps> = ({ product }) => {
  return (
    <Tr>
      <Td>{product.id}</Td>
      <Td>{product.title}</Td>
      <Td>{product.price}</Td>
      <Td>
        <Button variant="danger">Delete</Button>
      </Td>
    </Tr>
  )
}
