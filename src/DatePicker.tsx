import { setHours, setMinutes } from 'date-fns'
import addDays from 'date-fns/add_days/index.js'
import getDay from 'date-fns/get_day'
import React from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

export const BasicDatePicker: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }
  return <DatePicker selected={startDate} onChange={handleChange} />
}

export const SelectTime: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }
  return (
    <DatePicker
      selected={startDate}
      onChange={handleChange}
      showTimeSelect
      timeFormat="HH:mm"
    />
  )
}

export const SpecificDateRange: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }
  return (
    <DatePicker
      selected={startDate}
      onChange={handleChange}
      minDate={new Date()}
      maxDate={addDays(new Date(), 5)}
      placeholderText="Select a date between today and 5 days in the future"
    />
  )
}

export const SpecificTimeRange: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }
  return (
    <DatePicker
      selected={startDate}
      onChange={handleChange}
      showTimeSelect
      minTime={setHours(setMinutes(new Date(), 0), 17)}
      maxTime={setHours(setMinutes(new Date(), 30), 20)}
    />
  )
}

export const CustomDateFormat: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }
  return (
    <DatePicker
      selected={startDate}
      onChange={handleChange}
      dateFormat="yyyy/MM/dd"
    />
  )
}

export const FilterDays: React.FC = () => {
  const [startDate, setNewDate] = React.useState(new Date())
  const handleChange = (date: any) => {
    setNewDate(date)
  }

  const isWeekday = (date: Date) => {
    const day = getDay(date)
    return day !== 0 && day !== 6
  }
  return (
    <DatePicker
      selected={startDate}
      onChange={handleChange}
      filterDate={isWeekday}
    />
  )
}
