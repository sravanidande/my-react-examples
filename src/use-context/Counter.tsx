import 'bulma/css/bulma.css'
import React from 'react'

export interface CountContext {
  readonly count: number
  setCount?(count: number): void
}

const CountContext: React.Context<CountContext> = React.createContext<
  CountContext
>({ count: 0 })

export const DisplayCount: React.FC = () => {
  const { count } = React.useContext(CountContext)
  return <div className="text is-3">current counter count is:{count} </div>
}

export const IncrementCount: React.FC = () => {
  const { count, setCount } = React.useContext(CountContext)
  return (
    <button
      className="button"
      onClick={() => {
        if (setCount !== undefined) {
          // tslint:disable-next-line: restrict-plus-operands
          setCount(count + 1)
        }
      }}
    >
      {count}
    </button>
  )
}

export const Counter: React.FC = () => {
  const [count, setCount] = React.useState(0)

  return (
    <CountContext.Provider value={{ count, setCount }}>
      <DisplayCount />
      <IncrementCount />
    </CountContext.Provider>
  )
}
