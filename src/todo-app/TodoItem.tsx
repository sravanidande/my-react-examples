import React from 'react'

export interface TodoItemProps {
  readonly title: string
}

export const TodoItem: React.SFC<TodoItemProps> = ({ title }) => (
  <li>{title}</li>
)
