import React from 'react'
import { Button, Container, Title } from 'technoidentity-devfractal'
import { TodoItem, TodoItemProps } from './TodoItem'

interface TodoListProps {
  readonly todoList: ReadonlyArray<TodoItemProps>
  onDeleteTodo(index: number): void
}

export const TodoList: React.SFC<TodoListProps> = ({
  todoList,
  onDeleteTodo,
}) => {
  return (
    <Container>
      <Title>TodoList</Title>
      {todoList.map((todo, index) => (
        <>
          <TodoItem key={todo.title} title={todo.title} />
          <Button key={index} onClick={() => onDeleteTodo(index)}>
            Delete
          </Button>
        </>
      ))}
    </Container>
  )
}
