// ts-lint:disabled

import React from 'react'
import { Title } from 'technoidentity-devfractal'
import { useImmer } from 'use-immer'
import { AddTodoForm } from './AddTodoForm'
import { TodoList } from './TodoList'
// tslint:disable-next-line:readonly-array
const todoList = [
  { title: 'go for walk' },
  { title: 'read news' },
  { title: 'drink milk' },
]

export const Todo = () => {
  const [todos, setTodo] = useImmer(todoList)

  const handleTodo = (title: string) => {
    setTodo(draft => {
      draft.push({ title })
    })
  }

  const handleDelete = (index: number) => {
    setTodo(draft => {
      draft.splice(index, 1)
    })
  }

  return (
    <>
      <Title>hello</Title>
      <AddTodoForm onAddTodo={handleTodo} />
      <TodoList todoList={todos} onDeleteTodo={handleDelete} />
    </>
  )
}
