import React from 'react'
import { Simple } from 'technoidentity-devfractal'

interface TodoFormProps {
  onAddTodo(todo: string): void
}

export const AddTodoForm: React.SFC<TodoFormProps> = ({ onAddTodo }) => (
  <Simple.Form
    initialValues={{ title: '' }}
    onSubmit={(values, actions) => {
      onAddTodo(values.title)
      actions.setSubmitting(false)
    }}
  >
    <Simple.Text name="title" />
    <Simple.FormButtons />
  </Simple.Form>
)
