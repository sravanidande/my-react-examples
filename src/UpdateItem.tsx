import React from 'react'
import {
  Button,
  CheckBox,
  Table,
  TableBody,
  TableHead,
  Td,
  Th,
  Tr,
} from 'technoidentity-devfractal'

interface UpdateItem {
  readonly id: string
  readonly name: string
  readonly complete: boolean
}

interface UpdateItems {
  readonly updateItems: ReadonlyArray<UpdateItem>
  handleChange(id: string): void
}
export const UpdateListItems: ReadonlyArray<UpdateItem> = [
  { id: 'a', name: 'Learn React', complete: false },
  { id: 'b', name: 'Learn Firebase', complete: false },
  { id: 'c', name: 'Learn GraphQL', complete: false },
]

export const UpdateList: React.FC<UpdateItems> = ({
  updateItems,
  handleChange,
}) => {
  return (
    <Table>
      <TableHead>
        <Tr>
          <Th>Id</Th>
          <Th>Name</Th>
          <Th>Complete</Th>
        </Tr>
      </TableHead>
      <TableBody>
        {updateItems.map((item, index) => (
          <Tr key={index}>
            <Td>{item.id}</Td>
            <Td>{item.name}</Td>
            <Td>
              <CheckBox
                checked={item.complete}
                onChange={() => handleChange(item.id)}
              />
            </Td>
            <Td>
              <Button variant="danger">Delete</Button>
            </Td>
          </Tr>
        ))}
      </TableBody>
    </Table>
  )
}

export const UpdatedListItems: React.FC = () => {
  const [list, setList] = React.useState(UpdateListItems)
  const handleUpdate = (id: string) => {
    setList(
      list.map(item => {
        if (id === item.id) {
          return { ...item, complete: !item.complete }
        } else {
          return item
        }
      }),
    )
  }

  return (
    <UpdateList updateItems={UpdateListItems} handleChange={handleUpdate} />
  )
}
