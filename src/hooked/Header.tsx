import React from 'react'
import { Title } from 'technoidentity-devfractal'

interface HeaderProps {
  readonly title: string
}

export const Header: React.FC<HeaderProps> = ({ title }) => (
  <Title>{title}</Title>
)
