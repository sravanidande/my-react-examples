import React from 'react'
import { Column, Columns, Title } from 'technoidentity-devfractal'

export interface Movie {
  readonly Poster: string
  readonly Title: string
  readonly Year: number
}

export interface MovieProps {
  readonly movie: Movie
}

export const Movie: React.FC<MovieProps> = ({ movie }) => {
  return (
    <Column>
      <Title textAlignment="justified">{movie.Title}</Title>
      <img src={movie.Poster} />
      <Title textAlignment="justified">{movie.Year}</Title>
    </Column>
  )
}

export interface MovieListProps {
  readonly movieList: ReadonlyArray<Movie>
}

export const MovieList: React.FC<MovieListProps> = ({ movieList }) => {
  return (
    <Columns>
      {movieList.map((movie, index) => (
        <Movie key={index} movie={movie} />
      ))}
    </Columns>
  )
}
