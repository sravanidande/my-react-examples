import React from 'react'

interface SearchFormProps {
  onSearch(value: string): void
}

export const SearchForm: React.FC<SearchFormProps> = ({ onSearch }) => {
  const [search, setSearch] = React.useState('')
  const handleChange = (evt: any) => {
    setSearch(evt.target.value)
  }
  const handleSubmit = (e: any) => {
    e.preventDefault()
    onSearch(search)
    setSearch('')
  }

  return (
    <form>
      <input type="text" name="search" value={search} onChange={handleChange} />
      <input type="submit" onClick={handleSubmit} value="Search" />
    </form>
  )
}
