import React from 'react'

interface Product {
  readonly category: string
  readonly price: string
  readonly stocked: boolean
  readonly name: string
}

interface ProductCategoryRowProps {
  readonly category: string
}

export const ProductCategoryRow: React.FC<ProductCategoryRowProps> = ({
  category,
}) => (
  <tr>
    <th>{category}</th>
  </tr>
)

interface ProductRowProps {
  readonly name: string
  readonly price: string
  readonly stocked: boolean
}

export const ProductRow: React.FC<ProductRowProps> = ({
  name,
  price,
  stocked,
}) => {
  return (
    <tr>
      <td>{stocked ? name : <span style={{ color: 'red' }}>{name}</span>}</td>
      <td>{price}</td>
    </tr>
  )
}

interface ProductTableProps {
  readonly products: ReadonlyArray<Product>
  readonly filterText: string
  readonly inStockOnly: boolean
}

export const ProductsTable: React.FC<ProductTableProps> = ({
  products,
  filterText,
  inStockOnly,
}) => {
  const rows: any = []
  // tslint:disable-next-line: no-null-keyword
  let lastCategory: null | string = null

  products.forEach(product => {
    if (product.name.indexOf(filterText) === -1) {
      return
    }
    if (inStockOnly && !product.stocked) {
      return
    }
    if (product.category !== lastCategory) {
      rows.push(
        <ProductCategoryRow
          category={product.category}
          key={product.category}
        />,
      )
    }
    rows.push(
      <ProductRow
        name={product.name}
        price={product.price}
        stocked={product.stocked}
        key={product.name}
      />,
    )
    lastCategory = product.category
  })

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  )
}

interface SearchFormProps {
  readonly inStockOnly: boolean
  readonly filterText: string
  onFilterChange(evt: any): void
  onStockChange(evt: any): void
}

export const SerachForm: React.FC<SearchFormProps> = ({
  filterText,
  inStockOnly,
  onFilterChange,
  onStockChange,
}) => {
  return (
    <form>
      <input
        type="text"
        name="search"
        placeholder="Search...."
        value={filterText}
        onChange={(evt: any) => onFilterChange(evt.target.value)}
      />
      <p>
        <input
          type="checkbox"
          name="done"
          checked={inStockOnly}
          onChange={(evt: any) => onStockChange(evt.target.checked)}
        />

        <span> Only show products in stock</span>
      </p>
    </form>
  )
}

const products: ReadonlyArray<any> = [
  {
    category: 'Sporting Goods',
    price: '$49.99',
    stocked: true,
    name: 'Football',
  },
  {
    category: 'Sporting Goods',
    price: '$9.99',
    stocked: true,
    name: 'Baseball',
  },
  {
    category: 'Sporting Goods',
    price: '$29.99',
    stocked: false,
    name: 'Basketball',
  },
  {
    category: 'Electronics',
    price: '$99.99',
    stocked: true,
    name: 'iPod Touch',
  },
  {
    category: 'Electronics',
    price: '$399.99',
    stocked: false,
    name: 'iPhone 5',
  },
  { category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7' },
]

export const ProductApp: React.FC = () => {
  const [filterText, setFilterText] = React.useState('')
  const [inStockOnly, setInStockOnly] = React.useState(false)

  const handleFilterText = (text: string) => {
    setFilterText(text)
  }

  const handleStockOnly = (inStock: boolean) => {
    setInStockOnly(inStock)
  }

  return (
    <>
      <SerachForm
        filterText={filterText}
        inStockOnly={inStockOnly}
        onFilterChange={handleFilterText}
        onStockChange={handleStockOnly}
      />
      <ProductsTable
        products={products}
        filterText={filterText}
        inStockOnly={inStockOnly}
      />
    </>
  )
}
