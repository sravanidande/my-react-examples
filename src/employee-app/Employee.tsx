// const point = { x: 1, y: 2, z: 4 }
// const p1 = point
// p1.y = 3
// console.log(point)
// console.log(p1)

// const pi = produce(point, draft => {
//   draft.y = 3
// })

// const rect = {
//   top: { x: 1, y: 2 },
//   bottom: { x: 2, y: 3 },
// }

// const rect2 = { ...rect, bottom: { ...rect.bottom, y: 100 } }

// const ri = produce(rect, draft => {
//   draft.bottom.y = 100
// })

// console.log(rect2)

// const rects: ReadonlyArray<any> = [
//   {
//     top: { x: 1, y: 2 },
//     bottom: { x: 2, y: 3 },
//   },
//   {
//     top: { x: 2, y: 4 },
//     bottom: { x: 5, y: 6 },
//   },
//   {
//     top: { x: 2, y: 4 },
//     bottom: { x: 5, y: 5 },
//   },
//   {
//     top: { x: 2, y: 4 },
//     bottom: { x: 5, y: 4 },
//   },
//   {
//     top: { x: 2, y: 42 },
//     bottom: { x: 25, y: 24 },
//   },
// ]

// rects[2].bottom.y = 300

// const rects2 = [
//   ...rects.slice(0, 2),
//   { ...rects[2], bottom: { ...rects[2].bottom, y: 100 } },
//   ...rects.slice(3),
// ]

// tslint:disable

// const rsi = produce(rects, draft => {
//   rects[2].bottom.y = 300
// })

export type BranchType = 'narsaraopet' | 'hyderabad' | 'karimnagar'

export interface Employee {
  readonly id: number
  readonly name: string
  salary: number
  readonly branch: BranchType
}

const employees: Employee[] = [
  {
    id: 63,
    name: 'jgaku',
    salary: 6248,
    branch: 'hyderabad',
  },
  {
    id: 64,
    name: 'hkjsehl',
    salary: 8275,
    branch: 'karimnagar',
  },
  {
    id: 83,
    name: 'skhdfl',
    salary: 3793,
    branch: 'narsaraopet',
  },
  {
    id: 236,
    name: 'dhj',
    salary: 6354,
    branch: 'narsaraopet',
  },
  {
    id: 26,
    name: 'jkadhk',
    salary: 8243,
    branch: 'narsaraopet',
  },
]

employees[1].salary = 3000

const incrementSalaryBy: (sal: number, employees: Employee[]) => void = (
  sal,
  employees,
) => {
  for (let i = 0; i < employees.length; i += 1) {
    employees[i].salary += sal
  }
}

console.log(incrementSalaryBy(1000, employees))

const incrementSalaryOf: (sal: number, employees: Employee[]) => void = (
  sal,
  employees,
) => {
  for (let i = 0; i < employees.length; i += 1) {
    if (employees[i].branch === 'narsaraopet') {
      employees[i].salary += sal
    }
  }
}

console.log(incrementSalaryOf(1000, employees))
