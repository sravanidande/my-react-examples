import React from 'react'

const todos = ['run', 'eat', 'drink', 'sleep']

export interface TodoProps {
  readonly todo: string
}

export const TodoItem: React.FC<TodoProps> = ({ todo }) => {
  return <li>{todo}</li>
}

interface TodoListProps {
  readonly todos: ReadonlyArray<string>
}

export const TodoApp: React.FC = () => {
  const [todo, setTodo] = React.useState('')
  const [list, setList] = React.useState(todos)

  const handleChange = (evt: any) => {
    setTodo(evt.target.value)
  }
  const handleClick = (evt: any) => {
    evt.preventDefault()
    setList([...list, todo])
    setTodo('')
  }

  const handleRemove = (ind: number) => {
    const newList = list.filter((_, i) => i !== ind)
    setList(newList)
  }

  return (
    <>
      <ul>
        {list.map((todo, i) => (
          <>
            <TodoItem key={i} todo={todo} />
            <button className="close" onClick={() => handleRemove(i)}>
              x
            </button>
          </>
        ))}
      </ul>
      <form>
        <input type="text" name="todo" value={todo} onChange={handleChange} />
        <button type="submit" onClick={handleClick}>
          add
        </button>
      </form>
    </>
  )
}
